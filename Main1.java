import java.io.*;
import java.util.Scanner;

abstract class Vehicle implements Serializable
{
	Engine e=new Engine();
	protected String type;
	protected double number;
	protected String company;
	protected int model_no;
	protected int profile_no;


	public Vehicle(String str,double dp,int k,int f,String s1,int g,int q)
	{
		e.settorque(k);
		e.setcc(f);
		type=str;
		number=dp;
		company=s1;
		model_no=g;
		profile_no=q;
	}																																																																																																						

	public void settype(String sq)
	{
		type=sq;
	}

	public String gettype()
	{
		return type;
	}

	public void setrange(double d2)
	{
		number=d2;
	}

	public double getrange()
	{
		return number;
	}

	public int gettorque1()
	{
	  return e.gettorque();

	}

	public int getcc1()
	{
		return e.getcc();
	}


	public void setcompany(String s1)
	{
		company=s1;
	}

	public String getcompany()
	{
		return company;
	} 

	public void setmodel(int ki)
	{
		model_no=ki;
	}

	public int getmodel()
	{
		return model_no;
	}

	public int getprofile_no()
	{
		return profile_no;
	}
}

class Engine implements Serializable
{
	private int torque;
	private int cc;

	/*public Engine(int i,int j)
	{
      torque=i;
      cc=j;
	}*/

	public Engine()
	{

	}

	public void settorque(int io)
	{
      torque=io;
	}

	public int gettorque()
	{
		return torque;
	}

	public void setcc(int ij)
	{
		cc=ij;
	}

	public int getcc()
	{
		return cc;
	}
}

class Twowheeler extends Vehicle implements Serializable
{
	private int gear;
	private String pickup;

	public Twowheeler(String s1,double d1,int i1,int i2,String s2,int i3,int i4,int i5,String s3)
	{
		super(s1,d1,i1,i2,s2,i3,i4);

		gear=i5;
		pickup=s3;
	}

	public void setgear(int y)
	{
		gear=y;
	}

	public int getgear()
	{
		return gear;
	}

	public void setpickup(String str)
	{
		pickup=str;
	}

	public String getpickup()
	{
		return pickup;
	}

	public void display()
	{
		System.out.println("TWO_WHEELER");
	}
}

abstract class Fourwheeler extends Vehicle implements Serializable
{
	protected int gearbox;
	protected int milage;

	public Fourwheeler(String s,double d,int i,int n,String str,int t,int i1,int i2,int i3)
	{
		super(s,d,i,n,str,t,i3);

		gearbox=i1;
		milage=i2;
	}

	public void setgearbox(int ff)
	{
		gearbox=ff;
	}

	public int getgearbox()
	{
		return gearbox;
	}

	public void setmilage(int dd)
	{
		milage=dd;
	}

	public int getmilage()
	{
		return milage;
	}

}

class Private extends Fourwheeler implements Serializable
{
	private String aminities;

	public Private(String s1,double d1,int i1,int i2,String s2,int i3,int i4,int i5,int i6,String s3)
	{
		super(s1,d1,i1,i2,s2,i3,i4,i5,i6);
		aminities = s3;
	}

	public void setaminities(String str)
	{
		aminities=str;
	}

	public String getaminities()
	{
		return aminities;
	}

	public void display()
	{
		System.out.println("\nFOUR_WHEELER:(PRIVATE)");
	}
}

class commercial extends Fourwheeler implements Serializable
{
   private String meter;

   public commercial(String s1,double d1,int i1,int i2,String s2,int i3,int i4,int i5,int i6,String s3)
   {
      super(s1,d1,i1,i2,s2,i3,i4,i5,i6);
   	  meter = s3;
   }

   public void setmeter(String str)
   {
   	meter =str;
   }

   public String getmeter()
   {
   	return meter;
   }

   public void display()
   {
   	System.out.println("\nFOUR_WHEELER:(COMMERCIAL)");
   }
}

class Main1 implements Serializable
{
	public static boolean check_file(String filename) throws IOException
	{
		File f = new File(filename);

		if(f.exists())
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	public static void delete_file(String filename) throws IOException 
	{
		File f = new File(filename);

		if(f.delete())
		{
			System.out.println("Vehicle deleted");
		}
		else
		{
		    System.out.println("Sorry cannot complete action");	
		}
	}

	public static void choice()
	{
		System.out.println("\n1:New Profile");
		System.out.println("2:Exsisting Profile");
		System.out.println("3:Exit");
		System.out.print("Choice:");
	}

	public static void display()
	{
		System.out.println("\nUpdate Process:");
		System.out.println("1:TwoWheeler");
		System.out.println("2:FourWheeler");
		System.out.println("3:Exit");
		System.out.print("Choice:");
	}

	public static void display1()
	{
		System.out.println("\n1:Private");
		System.out.println("2:Commercial");
		System.out.print("Choice:");
	}

	public static void display2()
	{
		System.out.println("\n1:UPDATE COMPANY:");
		System.out.println("2:UPDATE MODEL:");
		System.out.println("3:View DETAILS:");
		System.out.println("4:DELETE DETAILS:");
		System.out.println("5:EXIT:");
	}


	public static void main(String []args) throws IOException
	{
		Scanner s1=new Scanner(System.in);

		int choice,option;
		int pr_num=1;

		while(true)

		{

			System.out.println("WELCOME");
		System.out.println("Feel Free and buy Vehicles");

		choice();
		choice=s1.nextInt();

		switch(choice)
		{
			case 1:
			{
				display();
				option=s1.nextInt();

				while(true)
				{
					if(check_file("My Vehicle/"+pr_num+".dat"))
					{
						pr_num++;
						continue;
					}
					else
					{
						break;
					}
				}

						
						switch(option)
						{
							case 1:
							{
								String str,str1,pick;
						double num;
						int mod,gear,torque,cc,gearbox,milage;

						System.out.print("\nTYPE(sports-normal):");
						str = s1.next();

						System.out.print("Number(pl.no):");
						num = s1.nextDouble();

						System.out.print("COMPANY:");
						str1 = s1.next();

						System.out.print("MODEL:");
						mod = s1.nextInt();

						System.out.print("GEAR(1-5):");
						gear = s1.nextInt();

						System.out.print("Pickup(0km-100km):");
						pick = s1.next();

						System.out.print("TORQUE:");
						torque = s1.nextInt();

						System.out.print("CC(horsepower):");
						cc = s1.nextInt();

						System.out.print("Gearbox(0 if Twowheeler):");
						gearbox = s1.nextInt();

						System.out.print("Milage(per liter):");
		      			milage = s1.nextInt();

						

                        	try
                        	{


                        	FileOutputStream fout = new FileOutputStream("My Vehicle/"+pr_num+".dat");
                        	ObjectOutputStream oOut = new ObjectOutputStream(fout);
                        	

                        	Twowheeler tw = new Twowheeler(str,num,torque,cc,str1,mod,pr_num,gear,pick);


                        	oOut.writeObject(tw);
                        	oOut.flush();
                        	oOut.close();

                        	System.out.println("\nSuccefully ADDED");
                        	System.out.println("Your profile no is:"+pr_num);
                        	System.out.println("Please use option to view");

                        	}
                        	catch(Exception e)
                        	{
                        	System.out.println("CRASH BOOM!!!!"+e);
                        	}
							break;
							}
							case 2:
						{
							int z;

                            	String str5,str12,pick1;
								double num1;
								int mod1,gear1,torque1,cc1,gearbox1,milage1;

								System.out.print("\nTYPE(sports-normal):");
								str5 = s1.next();

								System.out.print("Number(pl.no):");
								num1 = s1.nextDouble();

								System.out.print("COMPANY:");
								str12 = s1.next();

								System.out.print("MODEL:");
								mod1 = s1.nextInt();

								System.out.print("GEAR(1-5):");
								gear1 = s1.nextInt();

								System.out.print("Pickup(0km-1100km):");
								pick1 = s1.next();

								System.out.print("TORQUE:");
								torque1 = s1.nextInt();

								System.out.print("CC(horsepower):");
								cc1 = s1.nextInt();

								System.out.print("Gearbox(0 if Twowheeler):");
								gearbox1 = s1.nextInt();

								System.out.print("Milage(per liter):");
								milage1 = s1.nextInt();

                        		display1();

                        		z=s1.nextInt();

                        		switch(z)
                        		{
                        			case 1:
                        		{		
                        		String aminities;

                        		System.out.print("AMINITIES(e.g SUV):");
                        		aminities = s1.next();

                        		try
                        		{
                        			FileOutputStream fout = new FileOutputStream("My Vehicle/"+pr_num+".dat");
                        	        ObjectOutputStream oOut = new ObjectOutputStream(fout);

                        			Private p1 = new Private(str5,num1,torque1,cc1,str12,mod1,pr_num,gearbox1,milage1,aminities);
                                     
                        			

                        	        oOut.writeObject(p1);
                        	        oOut.flush();
                        	        oOut.close();

                        	       System.out.println("\nSuccefully ADDED");
                        	       System.out.println("Your profile no is:"+pr_num);
                        	       System.out.println("Please use option to view");

                        		}

                        		catch(Exception e)
                        		{
                        		  System.out.println("CRASH BOOM!!!!");	
                        		}

                        		break;
                        		}

                        			case 2:
                        		{
                        		String meter;

                        		System.out.print("METER(Digital-analog):");
                        		meter=s1.next();

                        		try
                        		{
                        			commercial c1 = new commercial(str5,num1,torque1,cc1,str12,mod1,pr_num,gearbox1,milage1,meter);

                        			FileOutputStream fout = new FileOutputStream("My Vehicle/"+pr_num+".dat");
                        	        ObjectOutputStream oOut = new ObjectOutputStream(fout);

                        	        oOut.writeObject(c1);
                        	        oOut.flush();
                        	        oOut.close();

                        	       System.out.println("\nSuccefully ADDED");
                        	       System.out.println("Your profile no is:"+pr_num);
                        	       System.out.println("Please use option to view\n");

                                 }
                            
                              catch(Exception e)
                              {
                            	System.out.println("CRASH BOOM!!!!");
                              }
                        		break;
                        		}
                            
                            	default:
                            	{
                            	System.out.println("Please enter valid choice");

                            	break;
                            	}
                        	}

						break;	
					}	

					case 3:System.exit(0);
						    break;
				}

				break;
			}

			case 2:
			{
				int port,choice2;
				boolean h;
			    do
			    {
			    	System.out.println("PROFILE(portfolio):");
			        port=s1.nextInt();

			        if(check_file("My Vehicle/"+port+".dat"))
			        {
			        	h=false;
			        }
			        else
			        {
                     System.out.println("INVALID!!!");
                     h=true;
			        }
			   
			    }while(h);
			    	//System.out.println("UPDATE PROFILE:");

			    	try
			    	{
			    		int flag=0,temp=0;

			    		FileInputStream fin = new FileInputStream("My Vehicle/"+port+".dat");
			    		ObjectInputStream oIn = new ObjectInputStream(fin);

			    		System.out.println("VEHICLE:");

			    		display();
			    		flag = s1.nextInt();

			    		/*if(flag==1)
			    		{
			    			Twowheeler tw2 = (Twowheeler)oIn.readObject();
			    		} */

			    		 if(flag==2)
			    		{
			    			display1();
			    			temp = s1.nextInt();
			    		}	

			    			/*if(temp==1)
			    			{
			    				Private p2 = (Private)oIn.readObject();
			    			}

			    			else if(temp==2)
			    			{
			    				commercial c2 = (commercial)oIn.readObject();
			    			}

			    			else
			    			{
			    				System.out.println("INVALID:");
			    			}
			    		}

			    			else
			    			{
			    				System.out.println("INVALID:");
			    			}*/

			    	    display2();

			    		choice2 = s1.nextInt();

			    		if(flag==1)
			    		{

			    			Twowheeler tw2 = (Twowheeler)oIn.readObject();

			    		FileOutputStream fout1 = new FileOutputStream("My Vehicle/"+tw2.getprofile_no()+".dat");
			    		ObjectOutputStream oOut1 = new ObjectOutputStream(fout1);

			    		switch(choice2)
			    		{
			    		case 1:
			    		{
			    			String co;

			    			System.out.println("RE-ENTER COMPANY:");
			    			co=s1.next();

			    			tw2.setcompany(co);
			    			System.out.println("COMPANY CHANGED:"+tw2.getcompany());


			    	  	oOut1.writeObject(tw2);
			    	  	oOut1.flush();
			    	  	oOut1.close();

			    	  	oIn.close();
			    	  	fin.close();
			    			
			    			break;
			    		}
			    		
			    		case 2:
			    		{
			    			int md;

			    			System.out.println("RE-ENTER MODEL:");
			    			md=s1.nextInt();

			    			tw2.setmodel(md);
			    			System.out.println("MODEL CHANGED:"+tw2.getmodel());


			    	  	oOut1.writeObject(tw2);
			    	  	oOut1.flush();
			    	  	oOut1.close();

			    	  	oIn.close();
			    	  	fin.close();
	                        break;		    			
   			    		}
                        
                        case 3:
                        {
                        	tw2.display();

                        	System.out.println("COMPANY:"+tw2.getcompany());
                        	System.out.println("MODEL:"+tw2.getmodel());
                        	//System.out.println("TORQUE:"+tw2.gettorque());
                        	//System.out.println("CC:"+tw2.getcc());
                        	System.out.println("NUMBER:"+tw2.getrange());
                        	System.out.println("GEAR:"+tw2.getgear());
                        	System.out.println("PICK_UP:"+tw2.getpickup());


			    	  	oOut1.writeObject(tw2);
			    	  	oOut1.flush();
			    	  	oOut1.close();

			    	  	oIn.close();
			    	  	fin.close();

                        	break;
                        }

                        case 4:
                        {
                        	oOut1.writeObject(tw2);
                        	oOut1.flush();
                        	oOut1.close();
                        	fout1.close();

                        	oIn.close();
                        	fin.close();

                        	delete_file("My Vehicle"+port+".dat");
                        	break;
                        }

                        case 5:System.exit(0);
                               break;
						
                        default:
                        {
                          System.out.println("ENTER VALID:");
                        }

			    	  }
			  
			    		}

			    		else if((flag==2)&&(temp==1))
			    		{

			    			Private p2 = (Private)oIn.readObject();
			    			FileOutputStream fout1 = new FileOutputStream("My Vehicle/"+p2.getprofile_no()+".dat");
			    		ObjectOutputStream oOut1 = new ObjectOutputStream(fout1);
			    		switch(choice2)
			    		{
			    		case 1:
			    		{
			    			String co;

			    			System.out.println("RE-ENTER COMPANY:");
			    			co=s1.next();

			    			p2.setcompany(co);
			    			System.out.println("COMPANY CHANGED:"+p2.getcompany());


			    	  	oOut1.writeObject(p2);
			    	  	oOut1.flush();
			    	  	oOut1.close();

			    	  	oIn.close();
			    	  	fin.close();
			    			break;
			    		}
			    		
			    		case 2:
			    		{

			    			int md;

			    			System.out.println("RE-ENTER MODEL:");
			    			md=s1.nextInt();

			    			p2.setmodel(md);
			    			System.out.println("MODEL CHANGED:"+p2.getmodel());
	                       
			    	  	oOut1.writeObject(p2);
			    	  	oOut1.flush();
			    	  	oOut1.close();

			    	  	oIn.close();
			    	  	fin.close();

	                        break;		   

   			    		}
                        
                        case 3:
                        {


                        	p2.display();

                        	System.out.println("COMPANY:"+p2.getcompany());
                        	System.out.println("MODEL:"+p2.getmodel());
                        	//System.out.println("TORQUE:"+p2.gettorque());
                        	//System.out.println("CC:"+p2.getcc());
                        	System.out.println("NUMBER:"+p2.getrange());
                        	System.out.println("GEAR:"+p2.getgearbox());
                        	System.out.println("PICK_UP:"+p2.getmilage());
                        	System.out.println("AMINITIES:"+p2.getaminities());

			    	  	oOut1.writeObject(p2);
			    	  	oOut1.flush();
			    	  	oOut1.close();

			    	  	oIn.close();
			    	  	fin.close();
                        	break;
                        }

                        case 4:
                        {

                        	oOut1.writeObject(p2);
                        	oOut1.flush();
                        	oOut1.close();
                        	fout1.close();

                        	oIn.close();
                        	fin.close();

                        	delete_file("My Vehicle"+port+".dat");

                        	break;
                        }

                        case 5:System.exit(0);
     					  		break;
                        

                        default:
                        {
                          System.out.println("ENTER VALID:");
                        }

			    	  }
			    	  
			    		}

			    		else if((flag==2)&&(temp==2))
			    		{
			    			commercial c2 = (commercial)oIn.readObject();
			    			FileOutputStream fout1 = new FileOutputStream("My Vehicle/"+c2.getprofile_no()+".dat");
			    		ObjectOutputStream oOut1 = new ObjectOutputStream(fout1);

			    		switch(choice2)
			    		{
			    		case 1:
			    		{
			    			String co;

			    			System.out.println("RE-ENTER COMPANY:");
			    			co=s1.next();

			    			c2.setcompany(co);
			    			System.out.println("COMPANY CHANGED:"+c2.getcompany());
			    			

			    	  	oOut1.writeObject(c2);
			    	  	oOut1.flush();
			    	  	oOut1.close();

			    	  	oIn.close();
			    	  	fin.close();
			    			break;
			    		}
			    		
			    		case 2:
			    		{


			    			int md;

			    			System.out.println("RE-ENTER MODEL:");
			    			md=s1.nextInt();

			    			c2.setmodel(md);
			    			System.out.println("MODEL CHANGED:"+c2.getmodel());
	                       	
			    	  	oOut1.writeObject(c2);
			    	  	oOut1.flush();
			    	  	oOut1.close();

			    	  	oIn.close();
			    	  	fin.close();

	                        break;		    			
   			    		}
                        
                        case 3:
                        {

                        	c2.display();

                        	System.out.println("COMPANY:"+c2.getcompany());
                        	System.out.println("MODEL:"+c2.getmodel());
                        	//System.out.println("TORQUE:"+c2.gettorque());
                        	//System.out.println("CC:"+c2.getcc());
                        	System.out.println("NUMBER:"+c2.getrange());
                        	System.out.println("GEAR:"+c2.getgearbox());
                        	System.out.println("PICK_UP:"+c2.getmilage());
                        	System.out.println("AMINITIES:"+c2.getmeter());

			    	  	oOut1.writeObject(c2);
			    	  	oOut1.flush();
			    	  	oOut1.close();

			    	  	oIn.close();
			    	  	fin.close();

                        	break;
                        }

                        case 4:
                        {

                        	oOut1.writeObject(c2);
                        	oOut1.flush();
                        	oOut1.close();
                        	fout1.close();

                        	oIn.close();
                        	fin.close();

                        	delete_file("My Vehicle"+port+".dat");

                        	break;
                        }

                        case 5:
                        {

                        	break;
                        }

                        default:
                        {
                          System.out.println("ENTER VALID:");
                          break;
                        }

			    	  }
			    	
			    		}
			    	 }
			    	catch(Exception e)
			    	{
			    		System.out.println("CRASH BOOM!!!"+e);
			    	}

				break;
			}
		}
		}

		
	}
}
