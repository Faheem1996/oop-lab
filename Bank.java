import java.util.*;
import java.io.*;
abstract class Account
{
	abstract void NewAccount()throws Exception;
	abstract int getAccount();
	abstract void deposit(float depositA);
	abstract boolean withdraw(float withdrawA);
	abstract float balanceEnquiry();
} 
class Savings extends Account
{
	private int accNo,intrest = 4 ;
	private float accBal;
	static BufferedReader read ;
	private float minBal = 500;
	void NewAccount() throws Exception
	{
		read = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter Account No- ");
		accNo = Integer.parseInt(read.readLine());
		while(true)
		{
			System.out.print("Enter Amount of First deposit - ");
			accBal = Float.parseFloat(read.readLine());
			if(accBal>=500)
				break ;
			else
				System.out.println("Must be greater than 500..");
		}
	}
	int getAccount()
	{
		return accNo;
	}
	void deposit(float depositA){
		accBal += depositA ;
	}
	boolean withdraw(float withdrawA){
		float temp ,balTemp;
		int month=1;
		System.out.print("Enter month:-") ;
		try{
			month=Integer.parseInt(read.readLine());
		}
		catch(Exception e)
		{
		}
		int monthIntrest = month/3 ;
		if(monthIntrest >= 1 )
		{
			balTemp=accBal*intrest/100 ;
		}
		temp = accBal - withdrawA ;
		if(temp >= 500 )
		{
			accBal += balTemp*monthIntrest ;
			accBal = temp ;
			return true ;
		}
		return false ;
	}
	float balanceEnquiry()
	{
		return accBal ;
	}
}
class Current extends Account
{
	private int accNo ;
	private float accBal;
	static BufferedReader read ;
	private float OlimitMin = 500 , overDraftbal = 10000000 ;
	void NewAccount() throws Exception
	{
		read = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter Account No- ");
		accNo = Integer.parseInt(read.readLine());
		System.out.print("Enter Amount to deposit - ");
		accBal = Float.parseFloat(read.readLine());
	}
	int getAccount()
	{
		return accNo;
	}
	void deposit(float depositA){
		float temp ;
		if(overDraftbal!=10000000)
		{
			temp = 10000000- overDraftbal;
			depositA-=temp ;
			overDraftbal +=temp ;
			if(depositA>0)
				accBal+=depositA ;
		}
		else
		{
			accBal += depositA ;
		}
	}
	boolean withdraw(float withdrawA){
		float temp,temp2 ;
		temp = accBal - withdrawA ;
		if(temp >= 0 )
		{
			accBal = temp ;
			return true ;
		}
		else
		{
			temp2 = overDraftbal + temp ;
			if(temp2 >= 0)
			{
			overDraftbal = temp2 ;
			return true ;
			}
			
		}
		return false ;
	}
	float balanceEnquiry()
	{
		return accBal ;
	}
}
class Bank
{
	static Account members[] = new Account[100];
	static int index = 0 ;
	static BufferedReader read ;
	public static void main(String args[])throws Exception
	{
		int choice ;
		read = new BufferedReader(new InputStreamReader(System.in));
		while(true)
		{
			System.out.println("\n\n--------------------------------------");
			System.out.println("\nPress 1 to create new Account.");
			System.out.println("Press 2 to Withdraw..");
			System.out.println("Press 3 to Deposit.");
			System.out.println("Press 4 to Check Balance.");
			System.out.println("Press 5 to Exit.");
			System.out.println("--------------------------------------");
			System.out.print("Enter your Choice :-");
			choice = Integer.parseInt(read.readLine());
			if(choice == 1 )
			{
				System.out.print("\n\nPress 1 to create Saving Account.");
				System.out.print("\nPress 2 to create Current Account.");
				choice = Integer.parseInt(read.readLine());
				if(choice == 1)
				{
					members[index] = new Savings();
				}
				else if(choice == 2 )
				{
					members[index]= new Current() ;
				}
				else
					System.out.print("\nWrong Choice Enter..");
				if(choice == 1 || choice == 2 )
				{
					members[index].NewAccount();
					index ++ ;
				}
			}
			else if(choice == 2 )
			{
				int accountNo ;
				float amounT ;
				boolean flag = true;
				System.out.print("Enter Account no:-");
				accountNo = Integer.parseInt(read.readLine());
				for(int i = 0 ; i < index ; i++ )
				{
					if(members[i].getAccount() == accountNo)
					{
						System.out.print("\nEnter Withdraw amount :- ");
						amounT = Float.parseFloat(read.readLine());
						if(members[i].withdraw(amounT))
						{
							System.out.print("\nsuccesfull..");
						}
						else
						{
							System.out.print("\nNot enough balace..");
						}
						flag = false ;
						break ;
					}
				}
				if(flag)
				{
					System.out.println("No Account.");
				}
			}
			else if(choice == 3 )
			{
				int accountNo ;
				float amounT;
				boolean flag = true;
				System.out.print("Enter Account no:-");
				accountNo = Integer.parseInt(read.readLine());
				for(int i = 0 ; i < index ; i++ )
				{
					if(members[i].getAccount() == accountNo)
					{
						System.out.print("\nEnter deposit amount :- ");
						amounT = Float.parseFloat(read.readLine());
						members[i].deposit(amounT) ;
						flag = false ;
						break ;
					}
				}
				if(flag)
				{
					System.out.println("No Account.");
				}
			}
			else if(choice == 4 )
			{
				int accountNo ;
				boolean flag = true;
				System.out.print("Enter Account no:-");
				accountNo = Integer.parseInt(read.readLine());
				for(int i = 0 ; i < index ; i++ )
				{
					if(members[i].getAccount() == accountNo)
					{
						System.out.println("Your Current Balance = "+ members[i].balanceEnquiry());
						flag = false ;
						break ;
					}
				}
				if(flag)
				{
					System.out.println("No Account.");
				}
			}
			else if(choice == 2 )
			{
				System.out.print("Bye Bye..");
				break ;
			}
			else
			{
				System.out.print("Wrong Choice Enter..");
			}
		}
	}
}