import java.io.*;
import java.util.Scanner;

public class FileHandling
{
	public int words=0;
	public int chars=0;
	public int lines=0;
	
	public static void Write(String FileName) throws IOException
	{
		FileWriter Fout = new FileWriter(FileName,true);
		char ch;
		while((ch=(char)System.in.read())!='\n')
		{
			Fout.write(ch);
		}
		Fout.close();
	}
	
	public static boolean Exist(String FileName) throws IOException
	{
		File f = new File(FileName);
		if(f.exists())
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	public static void Read(String FileName) throws IOException
	{
		FileReader Fin = new FileReader(FileName);
		int ch;
		while((ch=Fin.read())!=-1)
		{
			System.out.print((char)ch);
		}
		Fin.close();
	}	
	
	
	public static int Count(String FileName) throws IOException
	{
		
		Scanner ina = new Scanner(file);
		while(in.hasNextLine())  
    			{
    				lines++;
    				String line = in.nextLine();
   				for(int i=0;i<line.length();i++)
   		 		{
        				if(line.charAt(i)!=' ' && line.charAt(i)!='\n')
        				chars ++;
    		 		}
    				words += new StringTokenizer(line, " ,").countTokens();
			}
	}
	public static void Copy(String NewFile,String FileName) throws IOException
	{
		FileWriter Fout = new FileWriter(NewFile,true);
		FileReader Fin = new FileReader(FileName);
		int ch;
		while((ch=Fin.read())!=-1)
		{
			Fout.write((char)ch);
		}
		Fin.close();
		Fout.close();
	}
	
	public static void Rename(String OldName,String NewName) throws IOException
	{
		File Old_Name = new File(OldName);
		File New_Name = new File(NewName);
		if(Old_Name.renameTo(New_Name))
		{
			System.out.println("\nRename Successfully rename.\n");
		}
		else
		{
			System.out.println("\nERROR..........!!!!!!!!!!\n");
		}
	}
	
	public static void Delete(String FileName) throws IOException
	{
		File f = new File(FileName);
		if(f.delete())
		{
			System.out.println("\nFile delete successfully.\n");
		}
		else
		{
			System.out.println("\nERROR..........!!!!!!!!!!!!\n");
		}
	}
	
	
	public static void main (String[] args) throws IOException
	{
		Scanner input = new Scanner(System.in);
		String FileName;
		String NewName;
		String NewFile;
		int choice;
		int character = 0;
		int words = 0;
	
		System.out.println("\nEnter the name of file :  ");
		FileName = input.next();
		
		Count(FileName);
	
		while(true)
		{
			System.out.println("\n\n");
			System.out.println("\n1 : Read File");
			System.out.println("\n2 : Write File");
			System.out.println("\n3 : Count NO of Characters");
			System.out.println("\n4 : Count No of Words");
			System.out.println("\n5 : Count NO of lines");
			System.out.println("\n6 : Copy File");
			System.out.println("\n7 : Rename File");
			System.out.println("\n8 : Delete File");
			System.out.println("\n9 : Exit");
			System.out.println("\nEnter Your Choice");
			choice = input.nextInt();
			
			switch(choice)
			{
				case 1:
					if(Exist(FileName))
					{
						System.out.println("\n\nYour Data\n\n");
						Read(FileName);
					}
					else
					{
						System.out.println("\n\nFile Doesn't Exist.......!!!!!!!!\n\n");
					}
					break;
				
				case 2:
					Write(FileName);
					break;
				
				case 3:
					if(Exist(FileName))
					{
						System.out.println("\n\nNo of character in this file is : "+ character);
					}
					else
					{
						System.out.println("\n\nFile Doesn't Exist.......!!!!!!!!\n\n");
					}
					break;
				
				case 4:
					if(Exist(FileName))
					{
						System.out.println("\n\nNo of words in this file is : "+ words);
					}
					else
					{
						System.out.println("\n\nFile Doesn't Exist.......!!!!!!!!\n\n");
					}
					break;
				
				case 5:
					if(Exist(FileName))
					{
						System.out.println("\n\nNo of lines in this file is : "+ lines);
					}
					else
					{
						System.out.println("\n\nFile Doesn't Exist.......!!!!!!!!\n\n");
					}
					break;
				
				case 6:
					if(Exist(FileName))
					{
						System.out.println("\n\nEnter the new name of file in which you want to copy your data : ");
						NewFile = input.next();
						Copy(NewFile,FileName);
						System.out.println("\n\nFile copied successfully.\n\n");
					}
					else
					{
						System.out.println("\n\nFile Doesn't Exist.......!!!!!!!!\n\n");
					}
					break;
				
				case 7:
					if(Exist(FileName))
					{
						System.out.println("\n\nEnter the new name of file : ");
						NewName = input.next();
						Rename(FileName,NewName);
						FileName = NewName;
						System.out.println("\n\nFile rename successfully.");
					}
					else
					{
						System.out.println("\n\nFile Doesn't Exist.......!!!!!!!!\n\n");
					}
					break;
				
				case 8:
					if(Exist(FileName))
					{
						Delete(FileName);
					}
					else
					{
						System.out.println("\n\nFile Doesn't Exist.......!!!!!!!!\n\n");
					}
					break;
				
				case 9:
					return;
			}
		}
	}
}
