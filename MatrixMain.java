/*
3)	Program in Java to find A+B, A-B, A*B and transpose of A, where A is a matrix of 3*3 and B is a matrix of 3*4.
	Take the values in matrixes A and B from the user.
*/

import java.util.Scanner;

class Matrix
{
	int [] [] matrix;
	int row;
	int col;
	Matrix()
	{
	}
	Matrix(Matrix copy)
	{
		row = copy.row ;
		col = copy.col ;
		matrix = new int [row][col];
	}
	Matrix(int r,int c)
	{
		row = r;
		col = c;
		matrix = new int [row][col];
	}
	
	public void readMatrix()
	{
		Scanner scan = new Scanner(System.in);	
		System.out.print("Enter the row =" );
		row = scan.nextInt();
		System.out.print("Enter the column =" );
		col = scan.nextInt();
		
		matrix = new int [row][col];
		
		for(int i=0 ; i< row ; i++)
		{
			for(int j=0 ; j< col ; j++)
			{
				System.out.print( "arr ["+i+"] ["+j+"]=");
				matrix [i][j] = scan.nextInt();
			}
			System.out.println();
		}
	}	

	public void displayMatrix()
	{
		for(int i=0 ; i< row ; i++)
		{
			for(int j=0 ; j < col ; j++)
			{
				System.out.print( "\t" + matrix[i][j]);
			}
			System.out.println();
		}		
	}
	
	public void matrixAddition(Matrix matrix1,Matrix matrix2,Matrix matrix)
	{
		for(int i=0 ; i< matrix1.row ; i++)
		{
			for(int j=0 ; j<matrix1.row ; j++)
			{
				matrix.matrix [i][j] = matrix1.matrix [i][j] + matrix2.matrix[i][j];
			}
		}	
	}
	
	public void matrixMultiplication(Matrix matrix1,Matrix matrix2,Matrix matrix)
	{
		for(int i=0; i< matrix1.row ; i++)
		{	
			for(int j=0 ; j< matrix2.col; j++)
			{
				int sum=0;
				for(int k=0 ; k< matrix2.row ; k++)
				{
					sum += matrix1.matrix [i][k] * matrix2.matrix[k][j];
				}
				matrix.matrix [i][j] = sum;
			}
		}
	}

	public void matrixTranspose(Matrix matrix1)
	{
		for(int i=0 ; i < matrix1.row ; i++)
		{
			for(int j=0 ; j < matrix1.col ; j++)
			{
				this.matrix [i] [j] = matrix1.matrix [j] [i];
			}
		}
	}
	public void matrixScalar(Matrix matrixTemp , int scalar)
	{
		for(int i=0 ; i < row ; i++)
		{
			for(int j=0 ; j < col ; j++)
			{
				matrix [i] [j] =matrixTemp.matrix[i][j]* scalar;
			}
		}
	}
}


public class MatrixMain
{
	public static void main(String [] args)
	{
		char ch;
		Scanner scan =  new Scanner(System.in);
		boolean read = false ;
		
			Matrix matrix1=new Matrix();
			Matrix matrix2=new Matrix();
			Matrix matrix = new Matrix() ;
		do
		{
			System.out.println("*************************************************************************");
			System.out.println("* 1. Assign values to a matrix          				*");
			System.out.println("* 2. Extract values from a matrix       				*");
			System.out.println("* 3. Find transpose of a matrix     					*");
			System.out.println("* 4. Add two matrices after checking validity of the operation		*");
			System.out.println("* 5. Multiply two matrices after checking validity of the operation     *");
			System.out.println("* 6. Multiply a matrix with a scalar value   				*");
			System.out.println("* 7. Exit   								*");
			System.out.println("*************************************************************************");
			System.out.print("Enter your choice =");
			int choice = scan.nextInt();
			switch(choice)
			{
				case 1:
				{
					read = true;
					System.out.println("Enter First Matrix value\n");
					matrix1.readMatrix();
					System.out.println("Enter Second Matrix value\n");
					matrix2.readMatrix();
					break ;
				}
				case 2:
				{
					if(read)
					{
						System.out.println("First Matrix value\n");
						matrix1.displayMatrix();
						System.out.println("Second Matrix value\n");
						matrix2.displayMatrix();
					}
					else
					{
						System.out.println("First Assign the value to matrix..");
					}
					break ;
				}
				case 3:
				{
					if(read)
					{
						Matrix matrix3=new Matrix(matrix1);
						Matrix matrix4 = new Matrix(matrix2);
						System.out.println("First Transpose Matrix value\n");
						matrix3.matrixTranspose(matrix1);
						matrix4.matrixTranspose(matrix2);
						matrix3.displayMatrix();
						System.out.println("Second Transpose Matrix value\n");
						matrix4.displayMatrix();
					}
					else
					{
						System.out.println("First Assign the value to matrix..");
					}
					break;
				}
				case 4:
				{
					if(read)
					{
						if(matrix1.row==matrix2.row && matrix1.col==matrix2.col)
						{
							matrix = new Matrix(matrix1);
							matrix.matrixAddition(matrix1,matrix2,matrix);
							System.out.println("Addition of two Matrix\n");
							matrix.displayMatrix();
						}
						else
						{
							System.out.println("for addition rows and columns must be same");
						}
					}
					else
					{
						System.out.println("First Assign the value to matrix..");
					}
					break;
				}
				case 5:
				{
					if(read)
					{
						if(matrix1.col==matrix2.row)
						{
							matrix=new Matrix(matrix1.row,matrix2.col);
							matrix.matrixMultiplication(matrix1,matrix2,matrix);
							System.out.println("Multiplication of two Matrix\n");
							matrix.displayMatrix();
						}
						else
						{
							System.out.println(" columns of first matrix and rows of second matrix must be same for multiplication");
						}
					}
					else
					{
						System.out.println("First Assign the value to matrix..");
					}
					break;
				}
				case 6:
				{
					if(read)
					{
						int scalar ;
						System.out.print("\nEnter Scalar Value :- ") ;
						scalar = scan.nextInt() ;
						System.out.println("\n") ;
						Matrix matrix3=new Matrix(matrix1);
						Matrix matrix4 = new Matrix(matrix2);
						System.out.println("First Matrix with scalar value\n");
						matrix3.matrixScalar(matrix1,scalar);
						matrix4.matrixScalar(matrix2,scalar);
						matrix3.displayMatrix();
						System.out.println("Second Matrix with scalar value\n");
						matrix4.displayMatrix();
					}
					else
					{
						System.out.println("First Assign the value to matrix..");
					}
					break ;
				}
				case 7:
				{
					System.exit(0);
				}
				default:
				{
					System.out.println("Invalid choice ");
					break ;
				}
			}
		}while(true);
	}
}