/* 
*/

import java.io.*;
import java.util.Scanner;

abstract class Account implements Serializable      //Making Abstract class Account
{
	private String acc_num;
	protected double balance;
	protected String password;


	public Account(String s1,String pass,double bal)
	{
       acc_num=s1;
       password=pass;
       balance=bal;
	}

	public String getaccount()
	{
		return acc_num;
	}
	public String getpass()
	{
		return password;
	}

	public double getbalance()
	{
		return balance;
	}
	public boolean deposit(double money)
	{
		if(money>=0)
		{
			if(intrest())
		{
			balance=balance+money;
		}
		else
		{
			balance=balance+money;
		}
		return true;
		}
		else
		{
          System.out.println("Money cannot be deposited");
          return false;
		}
		
	}
	public abstract boolean withdraw(double wi_am);

	public void update(String s2)
	{
		password=s2;
		System.out.println("Your Password is changed");
	}

	public abstract boolean intrest();
}

 class Savings extends Account implements Serializable
{
	private float in_rate;
	private int date,month,year;

    public Savings(String str,String str1,double d1,float f1,int i2,int i3)
    {
    	super(str,str1,d1);
    	in_rate=f1;
    	month=i2;
    	year=i3;
    }
    public float get_rate()
    {
    	return in_rate;
    }

    public boolean intrest()
    {
    	Scanner s1=new Scanner(System.in);

    	int c1;
    	double intrest_am;

    	System.out.println("Please enter month");
        c1=s1.nextInt();

        if((c1>0)&&(c1<=12)&&(c1-month>=3))
        {
          do
          {
          	intrest_am=(balance*in_rate)/400;
            balance=balance+intrest_am;
            month=month+3;
          }	while(month<=c1);
          return true;
        }
        else
        {
        	System.out.println("Sorry quaterly intrest not calculated");
        	return false;
        }
    
    }

    public boolean withdraw(double wi_am)
    {
    	if((wi_am<=balance)&&(balance-wi_am>=500))
    	{
    		if(intrest())
    		{
    			balance=balance-wi_am;
    		}
    		else
    		{
    			balance=balance-wi_am;
    		}
    		return true;
    	}
    	else
    	{
    		System.out.println("Sorry request terminated");
    		return false;
    	}
    } 
}
 class Current extends Account implements Serializable 
{
	private double over_lim;

	public Current(String str,String str1,double d1,double d2)
	{
		super(str,str1,d1);
		over_lim=d2;
	}

	public double get_lim()
	{
		return over_lim;
	}

	public boolean withdraw(double wi_am)
	{
		if((wi_am<=balance+over_lim)&&wi_am>=0)
		{
			if(intrest())
			{
				balance=balance-wi_am;
			}
		    else
		   {
             balance=balance-wi_am;
		   }
		   return true;
		}
		else
		{
			System.out.println("Sorry overdraft also extended");
			return false;
		}
		
	}
	public boolean intrest()
	{
		System.out.println("Sorry as current account intrest cannot be calculated");
		return false;
	}

}

class Bank1 implements Serializable
{
	public static boolean file_check(String filename) throws IOException
	{
		File f = new File(filename);
		if(f.exists())
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	public static void file_delete(String filename) throws IOException
	{
		File f = new File(filename);
		if(f.delete())
		{
			System.out.println("Account deleted");
		}
		else
		{
		   System.out.println("Error occured");	
		}
	}

    public static void display()
    {
    	System.out.println("Welcome to HDFC");
    	System.out.println("We make banking feel like home");
    	System.out.println("1:New User??");
    	System.out.println("2:Exisiting User??");
    	System.out.println("3:Exit");
    	System.out.print("Choice:");
    }

    public static void display1()
    {
        System.out.println("1:Savings");
        System.out.println("2:Current");
        System.out.println("3:Exit");
        System.out.print("Choice:");
    }

    public static void display2()
    {
    	System.out.println("1:Add Money");
    	System.out.println("2:Withdraw Money");
    	System.out.println("3:Check Details");
    	System.out.println("4:Delete Account");
    	System.out.println("5:Exit");
    	System.out.print("Choice:");

    }
    public static void main(String []args) throws IOException
    {
    	Scanner s1=new Scanner(System.in);
    	int acc_num = 7;
    	int acc_num1 =13;
    	Long pass = 2550L;
    	Long pass1 = 1682L;

    	double str_balance;
    	double add_balance;
    	int choice,choice_1,m=0,y=0;
      while(true)
      {
    	display();
    	choice = s1.nextInt();
    	switch(choice)
    	{
    		case 1:
    		{
    			display1();
    			choice_1 = s1.nextInt();

    			if(choice_1==1)
    			{
    				do
    				{
    					
    					if(file_check("My Accounts/"+acc_num+".txt"))
    					{
    						acc_num+=7;
    						pass++;
    						continue;
    					}
    					else
    					{
    						break;
    					}
    				}while(true);
    				do{
    					System.out.println("Please enter starting balance");
    		            str_balance=s1.nextDouble();

    				    if(str_balance>1000)
    				    {
    				    	System.out.println("Please enter month of joining");
    				    	
    				    	m=s1.nextInt();

    				    	System.out.println("Please enter year of joining");
    				    	
    				    	y=s1.nextInt();
    				    }
    				    else
    				    {
    				    	System.out.println("Please enter atleast 1000 Rs to open a saving account");
    				    }
    				}while(str_balance<1000);

    				try
    				{
    					String acc_no = Integer.toString(acc_num);
    					String pwd = Long.toString(pass);

    					Savings a1=new Savings(acc_no,pwd,str_balance,5.0f,m,y);

    					FileOutputStream fout = new FileOutputStream("My Accounts/"+acc_num+".txt");
    					ObjectOutputStream oOut = new ObjectOutputStream(fout);

    					oOut.writeObject(a1);
    					oOut.flush();
    					oOut.close();

    					System.out.println("\nAccount_No:"+a1.getaccount());
    					System.out.println("Balance:"+a1.getbalance());
    					System.out.println("Password:"+a1.getpass());
    				}catch(Exception e)
    				{
    					System.out.println("Error Occured"+e);
    				}
    			}
    			else if(choice_1==2)
    			{

    					System.out.println("\nPlease enter starting balance");
    		            str_balance=s1.nextDouble();
    				do
    				{
    					
    					if(file_check("My Accounts/"+acc_num1+".txt"))
    					{
    						acc_num1+=13;
    						pass1++;
    						continue;
    					}
    					else
    					{
    						break;
    					}
    				}while(true);

    				try
    				{
    					String acc_no = Integer.toString(acc_num1);
    					String pwd = Long.toString(pass1);

    					Current c1 = new Current(acc_no,pwd,str_balance,1000000);

    					FileOutputStream fout = new FileOutputStream("My Accounts/"+acc_num1+".txt");
    					ObjectOutputStream oOut = new ObjectOutputStream(fout);

    					oOut.writeObject(c1);
    					oOut.flush();
    					oOut.close();

    					System.out.println("\nAccount_No:"+c1.getaccount());
    					System.out.println("Balance:"+c1.getbalance());
    					System.out.println("OverDraft Limit:"+c1.get_lim());
    					System.out.println("Password:"+c1.getpass());
    				}catch(Exception e)
    				{
    					System.out.println("\nError Occured"+e);
    				}

    			}
    			else
    			{
    				System.out.println("\nPlease enter valid choice");
    			}
    			break;
    		}

    		case 2:
    		{
    			int i,j;
    			String l1;
    			double dp;
    			boolean h;
    			do
    			{
    				System.out.print("\nPlease enter your account number");
    			    i=s1.nextInt();


    				if(file_check("My Accounts/"+i+".txt"))
    				{

                      h=false;
    				}
    				else
    				{
    					System.out.println("\nAccount does not exist or invalid try again");
    					h=true;
    				}
                                     
    			}while(h);
                System.out.print("\nPlease enter your Password:");
    			l1=s1.next();
                
                boolean g=true;
                if(g)
                {
    			 	if(i%7==0)
    			 	{
    			 		try
    			 		{

	                 FileInputStream fin = new FileInputStream("My Accounts/"+i+".txt");
	                 ObjectInputStream oIn = new ObjectInputStream(fin);

    			 			Savings s2=(Savings)oIn.readObject();
    				 	if(l1.equals(s2.getpass()))
    				 	{
    			 	 
                      FileOutputStream fout1 = new FileOutputStream("My Accounts/"+s2.getaccount()+".txt");
                      ObjectOutputStream oOut1 = new ObjectOutputStream(fout1);

                      display2();
                      
                      j=s1.nextInt();

                      switch(j)
                      {
                      	case 1:
                      	
                      		System.out.println("\nPlease enter Money you want to add");
                      		dp=s1.nextDouble();

                      		s2.deposit(dp);

                      		System.out.println("\nAmount sucessfully added");
                      		System.out.println("\nYour Current balance is:"+s2.getbalance());

                      		break;
                      	

                      	case 2:
                      	
                      		System.out.println("\nPlease enter money you want to withdraw");
                      		dp=s1.nextDouble();

                      		s2.withdraw(dp);

                      		System.out.println("\nAmount Sucessfully withdrawn");
                      		System.out.println("\nYour Current balance is:"+s2.getbalance());

                      		break;
                      	

                      	case 3:
                      	
                      		System.out.println("\nYour Account Details:");
                      		System.out.println("\nAccount_Number:"+s2.getaccount());
                      		System.out.println("\nCurrent_Balance:"+s2.getbalance());
                      		break;
                      	

                      	case 4:
                      	
                      		oOut1.writeObject(s2);
                      		oOut1.flush();
                      		oOut1.close();
                      		fout1.close();

                      		oIn.close();
                      		fin.close();

                      		file_delete("My Accounts/"+s2.getaccount()+".txt");

                      		break;
                      	

                      	case 5:  System.exit(0);
                      	         break;   
                      	
                      		//return;
                      	//break;
                      	

                    	default:
                      	
                      		System.out.println("\nPlease enter valid inputs");
                      		

                      }
                  
                      while(j!=4)
                      {
                           

                    oOut1.writeObject(s2);
						        oOut1.flush();

						        oOut1.close();
						        fout1.close();

						        oIn.close();
						        fin.close();
						   		   
                      }
    			 	}
    			 	else
    			 	{
                      System.out.println("\nPlease enter valid password");

                      oIn.close();
                      fin.close();
                      
    			 	}
    			 }
    			 catch(Exception e)
    			 {
    			 	System.out.println("\nTransition Sucessfull");
    			 }
    	}
    			  else if(i%13==0)
    			  {
    			  	try
    			  		{

                 FileInputStream fin = new FileInputStream("My Accounts/"+i+".txt");
                 ObjectInputStream oIn = new ObjectInputStream(fin);

    			  		Current c2=(Current)oIn.readObject();
    			  		if(l1.equals(c2.getpass()))
    			  	{
                      FileOutputStream fout1 = new FileOutputStream("My Accounts/"+c2.getaccount()+".txt");
                      ObjectOutputStream oOut1 = new ObjectOutputStream(fout1);

                      display2();
                      
                      j=s1.nextInt();

                      switch(j)
                      {
                      	case 1:
                      	{
                      		System.out.println("\nPlease enter Money you want to add");
                      		dp=s1.nextDouble();

                      		c2.deposit(dp);

                      		System.out.println("\nAmount sucessfully added");
                      		System.out.println("\nYour Current balance is:"+c2.getbalance());

                      		break;
                      	}
                      	case 2:
                      	{
                            
                      		System.out.println("\nPlease enter money you want to withdraw");
                      		dp=s1.nextDouble();

                      		c2.withdraw(dp);

                      		System.out.println("\nAmount Sucessfully withdrawn");
                      		System.out.println("\nYour Current balance is:"+c2.getbalance());

                      		break;
                      	}

                      	case 3:
                      	{
                      		System.out.println("\nYour Account Details:");
                      		System.out.print("\nAccount_Number:"+c2.getaccount());
                      		System.out.print("\nCurrent_Balance:"+c2.getbalance());
                      		break;
                      	}


                      	case 4:
                      	{
                      		oOut1.writeObject(c2);
                      		oOut1.flush();
                      		oOut1.close();
                      		

                      		oIn.close();
                      		

                      		file_delete("My Accounts/"+c2.getaccount()+".txt");

                      		break;
                      	}

                      	
                      	case 5:
                      	
                      		return;
                      	//break;
                		
                		default:
                      	{
                      		System.out.println("\nPlease enter valid inputs");
                      		break;
                      	}

                      }
                      while(j!=4)
                      {
                             
                      	   oOut1.writeObject(c2);
						   oOut1.flush();
						   oOut1.close();
						   fout1.close();

						   oIn.close();
						   fin.close();
						   
                      }
    			  	}
    			  	else
    			  	{
    			  		System.out.println("\n Enter the valid password");
    			  		oIn.close();
    			  		fin.close();
    			  		
    			  	}

    			  }
    			  catch(Exception e)
    			  {
    			  	System.out.println("\nTransition is done Sucessfully ");
    			  }

    			  }
    			    			  
                }

                break;
    		}

    		case 3:
    		{
    			return;
    			
    		}
    			default:
    			  {
    			  	System.out.println("\nPlease enter valid choice");
    			  	break;
    			  }
    	}

     }

    }
}