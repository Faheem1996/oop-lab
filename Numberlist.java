import java.util.*;
class List
{
	Scanner read = new Scanner(System.in);
	private int element;
	List()
	{
	}
	List(int element)
	{
	this.element = element;
	}
	int getElement()
	{
		return element;
	}
	void readElement()
	{
	System.out.print("Enter a number = ");
	element = read.nextInt();
	}
	boolean findMatch(int element)
	{
		if(this.element == element )
			return true;
		else
			return false ;
	}
	void displayElement(int index)
	{
		System.out.println("Element of" + (index + 1) + " = " + element );
	}
	void setElement(int e)
	{
		element = e ;
	}
	
}
class Numberlist
{
	static Scanner read = new Scanner(System.in);
	static int index = -1 ;
	public static void main(String [] args)
	{
		int temp ;
		System.out.print("Enter Size of Array :- ");
		int size= read.nextInt();
		List mylist[] = new List[size] ;
		while(true)
		{
			System.out.println("=====================================================================");
			System.out.println("\t\t\t Menu");
			System.out.println("=====================================================================");
			System.out.println(" Press 1 to Add numbers ");
			System.out.println(" Press 2 to Delete an number from list ");
			System.out.println(" Press 3 to Check whether the list contains duplicate elements");
			System.out.println(" Press 4 to Remove duplicate values of elements from the list");
			System.out.println(" Press 5 to Sort the list in ascending order ");
			System.out.println(" Press 6 to Sort the list in descending order");
			System.out.println(" Press 7 to Reverse the list");
			//System.out.println(" Press 9 to Sort the list in descending order");
			System.out.println(" Press 8 to Calculate maximum, minimum, sum, mean, median, mode, and standard deviation");
			System.out.println(" Press 9 to display");
			System.out.print(" Press 10 to Exit = ");
			int choice = read.nextInt();
			if(choice == 1 )
			{
				if(index < size-1 )
				{
					mylist[++index] = new List() ;
					mylist[index].readElement();
				}
				else
				{
					System.out.println("Array size out of bound..");
				}
			}
			else if(choice == 2 )
			{
				System.out.print("Enter number which you want to delete : ");
				temp = read.nextInt() ;
				deleteElement(mylist,temp) ;
			}
			else if(choice == 3 )
			{
				
				checkDuplicate(mylist);
			}
			else if(choice == 4 )
			{
				removeDuplicate(mylist);
			}
			else if(choice == 5 )
			{
				sortAscending(mylist) ;
			}
			else if(choice == 6 )
			{				
				sortDecendig(mylist) ;
			}
			else if(choice == 7 )
			{
				reverse(mylist);
			}
			else if(choice == 8 )
			{
				statistics(mylist) ;
			}
			else if(choice == 9 )
			{
				displayAll(mylist) ;
			}
			else if(choice == 10 )
			{
				break ;
			}
			else
			{
				System.out.println("Wrong Choice..");
			}
			
		}
	}
	public static void statistics(List [] mylist)
	{
		float mean ,sum = 0, median=0 , mode ,max = 0 , min = mylist[index].getElement() ;
				for(int i = 0 ; i <=index ; i++)
				{
					sum+=mylist[i].getElement() ;
					if(max < mylist[i].getElement())
					{
						max = mylist[i].getElement() ;
					}
					if(min > mylist[i].getElement())
					{
						min = mylist[i].getElement() ;
					}
				}
				mean=sum/(index+1) ;
				sortAscending(mylist);
				if( index%2 == 0 )
					median = mylist[(index/2 )].getElement() ;
				else
					median = mylist[(index/2)].getElement() + mylist[index/2+1].getElement()/2 ;
				
				System.out.println("Mean = " + mean + ", Median = " + median + ", Max =" + max + " , Min = " + min + " , Sum = " + sum +" , SD = " + Math.sqrt(mean) );
	}
	public static void checkDuplicate(List [] mylist)
	{
		int count = 0 ,templistIndex = -1;
		int templist[] = new int [index+1] ;
		
		for(int i = 0 ; i <= index ; i++)
		{
			count = 0 ;
			boolean flag = true ;
			for(int k = 0 ; k <= templistIndex ; k++)
			{
				if(templist[k] == mylist[i].getElement())
				{
					flag = false;
					break ;
				}
			}
			if(flag)
			{
				System.out.print("Duplicate of " + mylist[i].getElement() + " is :- ");
				for(int j = i ; j <=index ; j ++ )
				{
					if(mylist[i].getElement() == mylist[j].getElement())
					{
						count ++ ;
					}
				}
				System.out.println(count-1) ;
			}
			templist[++templistIndex] = mylist[i].getElement() ;
		}
	}
	public static void reverse(List [] mylist)
	{
		int t ;
		for(int i = 0,j = index ; i < (index/2) ; i ++ ,j--)
		{
			t = mylist[i].getElement();
			mylist[i].setElement(mylist[j].getElement());
			mylist[j].setElement(t);
		}
	}
	public static void removeDuplicate(List [] mylist)
	{
		for(int i = 0 ; i <= index ; i ++ )
		{
			for(int j = i + 1 ; j<index ; j ++ )
			{
				if(mylist[i].findMatch(mylist[j].getElement()))
				{
					deleteElement(mylist,mylist[j].getElement());
				}
			}
		}
	}
	public static void displayAll(List [] mylist)
	{
		for(int i = 0 ; i <= index ; i++ )
		{
			mylist[i].displayElement(i) ;
		}
	}
	public static void sortAscending( List [] mylist )
	{
		for(int i = 0 ; i < index; i++ )
		{
			for(int j = 0 ; j < index-i ; j ++) 
			{
				if(mylist[j].getElement() > mylist[j+1].getElement())
				{
					int swap = mylist[j].getElement();
					mylist[j].setElement(mylist[j+1].getElement()) ;
					mylist[j+1].setElement(swap) ;
				}
			}
		}
	}
	public static void sortDecendig(List [] mylist )
	{
		for(int i = 0 ; i < index; i++ )
		{
			for(int j = 0 ; j < index-i ; j ++) 
			{
				if(mylist[j].getElement() < mylist[j+1].getElement())
				{
					int swap = mylist[j].getElement();
					mylist[j].setElement(mylist[j+1].getElement());
					mylist[j+1].setElement(swap );
				}
			}
		}
	}
	public static void deleteElement(List [] mylist,int temp)
	{
		boolean flag = false ;
		for(int i=0 ; i <= index ; i ++ )
		{
			if(mylist[i].findMatch(temp))
			{
				for(int j= 0 ; j <= index ; j ++) 
				{
					mylist[j].setElement(mylist[j+1].getElement()) ;
					flag = true ;
					index -- ;
				}
			}
		}
		if(!flag)
		{
			System.out.println("No match found..");
		}
	}
}